﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SIPlayHomework.Models
{
    public class Registration
    {
        public int ID { get; set; }

        [ForeignKey("Family")]
        public int? UserId { get; set; }
        public int SessionId { get; set; }
        public decimal? Price { get; set; }
        public virtual Session Session { get; set; }
        public virtual Family Family { get; set; }
    }
}