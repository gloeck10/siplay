﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SIPlayHomework.Models;
using Microsoft.AspNet.Identity;

namespace SIPlayHomework.Controllers
{
    [Authorize]
    public class SessionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Session
        public ActionResult Index()
        {
            return View(db.Sessions.ToList());
        }

        // GET: Session/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session session = db.Sessions.Find(id);
            if (session == null)
            {
                return HttpNotFound();
            }
            return View(session);
        }

        // GET: Session/Create
        public ActionResult Create()
        {
            ViewBag.AllowRegister = list;
            return View();
        }

        // POST: Session/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,StartDate,AllowRegister,Price")] Session session)
        {
            if (ModelState.IsValid)
            {
                db.Sessions.Add(session);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AllowRegister = list;
            return View(session);
        }

        // GET: Session/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session session = db.Sessions.Find(id);
            if (session == null)
            {
                return HttpNotFound();
            }
            ViewBag.AllowRegister = list;
            return View(session);
        }

        // POST: Session/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,StartDate,AllowRegister,Price")] Session session)
        {
            if (ModelState.IsValid)
            {
                db.Entry(session).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AllowRegister = list;
            return View(session);
        }

        // GET: Session/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session session = db.Sessions.Find(id);
            if (session == null)
            {
                return HttpNotFound();
            }
            return View(session);
        }

        // POST: Session/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Session session = db.Sessions.Find(id);
            db.Sessions.Remove(session);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Session/Register/5
        public ActionResult Register(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session session = db.Sessions.Find(id);
            if (session == null)
            {
                return HttpNotFound();
            }

            var user = User.Identity.GetUserId();
            var familyMembers = db.Families.Where(x => x.AspNetUserId == user);
            if (session.AllowRegister == "Child")
            {
                familyMembers = familyMembers.Where(x => x.Child == true);
            }
            else if (session.AllowRegister == "Adult")
            {
                familyMembers = familyMembers.Where(x => x.Child == false);
            }

            ViewBag.FamilyMembers = familyMembers;
            return View(session);
        }

        [HttpPost]
        public PartialViewResult Enroll(int sessionId, int? familyMemberId, decimal price)
        {

            try
            {
                var registration = new Registration {  SessionId = sessionId, UserId = familyMemberId, Price= price };
                db.Registrations.Add(registration);
                db.SaveChanges();
                ViewBag.result = "Success";
            }
            catch (Exception)
            {
                ViewBag.result = "Error";                
            }
            return PartialView("_EnrollResult");
        }

        SelectList list = new SelectList(new[]
        {
            new { ID = "Adult", Name = "Adult" },
            new { ID = "Both", Name = "Both" },
            new { ID = "Child", Name = "Child" },
        },
        "ID", "Name", 1);



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
