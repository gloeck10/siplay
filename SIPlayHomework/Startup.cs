﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SIPlayHomework.Startup))]
namespace SIPlayHomework
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
