﻿using SIPlayHomework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIPlayHomework.Repository
{
  
    public interface ISIPlayRepository
    {
        List<Session> GetAllSessions();
        Session GetSessionById(int? id);
        void AddSession(Session session);
        void UpdateSession(Session session);
        void DeleteSession(Session session);
        //List<Account> GetAllAccounts();
        //Account GetAccountById(int? id);
        //void AddAccount(Account account);
        //void UpdateAccount(Account account);
        //void DeleteAccount(Account account);

        //List<AccountMember> GetAllAccountMembers();
        //AccountMember GetAccountMemberById(int? id);
        //void AddAccountMember(AccountMember account);
        //void UpdateAccountMember(AccountMember account);
        //void DeleteAccountMember(AccountMember account);
        void Save();
    }
}
