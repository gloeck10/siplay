﻿using SIPlayHomework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIPlayHomework.Repository
{
    public class DBWorker
    {
        private Entities entities = null;

        // This will be called from controller default constructor
        public DBWorker()
        {
            entities = new Entities();
            SIPlayRepository = new SIPlayRepository(entities);
        }

        // This will be created from test project and passed on to the
        // controllers parameterized constructors
        public DBWorker(ISIPlayRepository siPlayRepo)
        {
            SIPlayRepository = siPlayRepo;
        }

        public ISIPlayRepository SIPlayRepository
        {
            get;
            private set;
        }
    }
}