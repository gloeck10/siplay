﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIPlayHomework.Models;

namespace SIPlayHomework.Repository
{
    public class SIPlayRepository : ISIPlayRepository
    {
        Entities entities = null;

        public SIPlayRepository(Entities entities)
        {
            this.entities = entities;
        }

        #region sessions
        public void AddSession(Session session)
        {
            entities.Session.Add(session);
        }

        public void DeleteSession(Session session)
        {
            entities.Session.Remove(session);
        }

        public List<Session> GetAllSessions()
        {
            return entities.Session.ToList();
        }

        public Session GetSessionById(int? id)
        {
            return entities.Session.SingleOrDefault(x => x.SessionId == id);
        }



        public void UpdateSession(Session session)
        {
            entities.Session.Attach(session);
            entities.Entry(session).State = System.Data.Entity.EntityState.Modified;
            Save();
        }
        #endregion

        #region family/account
        public void AddAccount(Account account)
        {
            entities.Account.Add(account);
        }

        public void DeleteAccount(Account account)
        {
            entities.Account.Remove(account);
        }

        public List<Account> GetAllAccounts()
        {
            return entities.Account.ToList();
        }

        public Account GetAccountById(int? id)
        {
            return entities.Account.SingleOrDefault(x => x.AccountId == id);
        }



        public void UpdateAccount(Account account)
        {
            entities.Account.Attach(account);
            entities.Entry(account).State = System.Data.Entity.EntityState.Modified;
            Save();
        }
        #endregion

        #region family/account member
        public void AddAccountMember(AccountMember accountMember)
        {
            entities.AccountMember.Add(accountMember);
        }

        public void DeleteAccountMember(AccountMember accountMember)
        {
            entities.AccountMember.Remove(accountMember);
        }

        public List<AccountMember> GetAllAccountMembers()
        {
            return entities.AccountMember.Include("Account").Include("AccountMemberType").ToList();
        }

        public AccountMember GetAccountMemberById(int? id)
        {
            return entities.AccountMember.SingleOrDefault(x => x.AccountMemberId == id);
        }

        public void UpdateAccountMember(AccountMember accountMember)
        {
            entities.AccountMember.Attach(accountMember);
            entities.Entry(accountMember).State = System.Data.Entity.EntityState.Modified;
            Save();
        }
        #endregion

        public void Save()
        {
            entities.SaveChanges();
        }
    }


}