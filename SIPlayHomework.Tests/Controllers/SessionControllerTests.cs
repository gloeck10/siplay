﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SIPlayHomework.Controllers;
using SIPlayHomework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SIPlayHomework.Controllers.Tests
{
    [TestClass()]
    public class SessionControllerTests
    {
        [TestMethod()]
        public void DetailsTest()
        {
            var controller = new SessionController();
            var result = controller.Details(2) as ViewResult;
            var session = (Session)result.ViewData.Model;
            Assert.AreEqual("Session 2", session.Name);
        }
    }
}